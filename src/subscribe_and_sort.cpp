#include <ros/ros.h>
#include <std_msgs/String.h>

void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
    std::string unsorted = msg->data;
    std::sort(unsorted.begin(), unsorted.end());
    ROS_INFO("Original: [%s]", msg->data.c_str());
    ROS_INFO("Sorted  : [%s] \n", unsorted.c_str());
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "subscribe_and_sort");
    ros::NodeHandle nh;

    ros::Subscriber sorting_sub = nh.subscribe("/hello", 1, chatterCallback);
    ros::Publisher chatter_pub = nh.advertise<std_msgs::String>("/sorted", 1);


    ros::spin();

    return 0;
}
