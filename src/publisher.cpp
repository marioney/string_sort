#include <ros/ros.h>
#include <std_msgs/String.h>
#include <string_sort/EnablePub.h>


class Publisher
{
	public:
	// Flag for controlling whether or no to publish
    bool publish_flag;
    // Callback for enable/disable publish service
    bool enable_publishing_callback(string_sort::EnablePub::Request  &req,
             string_sort::EnablePub::Response &res);
    // Generates a random string of size length
    std::string random_string( size_t length );
    
};
std::string Publisher::random_string( size_t length )
{
    auto randchar = []() -> char
    {
        const char charset[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length, 0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}

bool Publisher::enable_publishing_callback(string_sort::EnablePub::Request  &req,
         string_sort::EnablePub::Response &res)
{
  
  if (publish_flag)
  {
    ROS_INFO("[String Publisher] -- Disabling publisher"); 
    publish_flag = false;
  }
  else
  {
    ROS_INFO("[String Publisher] -- Enabling publisher");
    publish_flag = true;
  }
  return true;
}



int main(int argc, char **argv)
{
    ros::init(argc, argv, "string_publisher");
    ros::NodeHandle nh;

	Publisher my_publisher;
	// Set initial value for publish flag
    my_publisher.publish_flag = true;
    ros::Publisher chatter_pub = nh.advertise<std_msgs::String>("/hello", 1);
    ros::ServiceServer service = nh.advertiseService("enable_disable_pub", &Publisher::enable_publishing_callback, &my_publisher);
    int string_length;
    nh.param("string_length", string_length, 10);

    ros::Rate loop_rate(1);\

    ROS_INFO("[String Publisher] -- Start publishing random string");
    while (ros::ok())
    {
        std_msgs::String msg;
        msg.data = my_publisher.random_string(10);
        
        if (my_publisher.publish_flag)
                chatter_pub.publish(msg);

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
