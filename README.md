# String sort package

This package contains two nodes.

### string_publisher

Publishes a random string in the /hello (std_msgs/String) topic, and provides a service (enable_disable_pub) to stop or restart this publication.

### string_sorter

Subscribes to the random string and sorts it. Then publishes the result in another topic (/sorted)

## Compile

Compilation is done using catkin_make

```

catking_make string_sort
```

## Launch



A launch file `sort_string.launch`  is included, it launches both the publisher and the subscriber nodes. Launch using


```

roslaunch string_sort sort_string.launch
```

## Enable / disable publishing

Call the service enable_disable_pub to stop or restart the publication of the string message.

```

rosservice call /enable_disable_pub 
```





